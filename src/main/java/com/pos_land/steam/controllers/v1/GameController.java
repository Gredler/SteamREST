package com.pos_land.steam.controllers.v1;

import com.pos_land.steam.api.model.GameDTO;
import com.pos_land.steam.services.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(GameController.BASE_URL)
public class GameController {
    public static final String BASE_URL = "api/v1/game";

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<GameDTO> getAllGames() {
        return gameService.getSortedGames();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public GameDTO getGameById(@PathVariable String id) {
        return gameService.getById(new Long(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteGame(@PathVariable String id) {
        gameService.deleteById(new Long(id));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public GameDTO updateGame(@PathVariable Long id, @RequestBody GameDTO gameDTO) {
        return gameService.saveGameByDTO(id, gameDTO);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public GameDTO createNewGame(@RequestBody GameDTO gameDTO) {
        return gameService.createNewGame(gameDTO);
    }

    @PatchMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public GameDTO patchGame(@PathVariable Long id, @RequestBody GameDTO gameDTO) {
        return gameService.saveGameByDTO(id, gameDTO);
    }
}
