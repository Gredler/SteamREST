package com.pos_land.steam.controllers.v1;

import com.pos_land.steam.api.model.PublisherDTO;
import com.pos_land.steam.services.PublisherService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(PublisherController.BASE_URL)
public class PublisherController {
    public final static String BASE_URL = "api/v1/publisher";

    private final PublisherService publisherService;

    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<PublisherDTO> getAllPublishers() {
        return publisherService.getSortedPublishers();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PublisherDTO getPublisherById(@PathVariable String id) {
        return publisherService.getById(new Long(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deletePublisher(@PathVariable String id) {
        publisherService.deleteById(new Long(id));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public PublisherDTO updatePublisher(@PathVariable Long id, @RequestBody PublisherDTO publisherDTO) {
        return publisherService.savePublisherByDTO(id, publisherDTO);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PublisherDTO createNewPublisher(@RequestBody PublisherDTO publisherDTO) {
        return publisherService.createNewPublisher(publisherDTO);
    }

    @PatchMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public PublisherDTO patchPublisher(@PathVariable Long id, @RequestBody PublisherDTO publisherDTO) {
        return publisherService.savePublisherByDTO(id, publisherDTO);
    }
}
