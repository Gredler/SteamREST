package com.pos_land.steam.controllers.v1;

import com.pos_land.steam.api.model.LanguageDTO;
import com.pos_land.steam.services.LanguageService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = LanguageController.BASE_URL)
public class LanguageController {
    public static final String BASE_URL = "api/v1/language";

    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<LanguageDTO> getAllLanguages() {
        return languageService.getSortedLanguages();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LanguageDTO getLanguageById(@PathVariable String id) {
        return languageService.getById(new Long(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteLanguage(@PathVariable String id) {
        languageService.deleteById(new Long(id));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public LanguageDTO updateLanguage(@PathVariable Long id, @RequestBody LanguageDTO languageDTO) {
        return languageService.saveLanguageByDTO(id, languageDTO);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LanguageDTO createNewLanguage(@RequestBody LanguageDTO languageDTO) {
        return languageService.createNewLanguage(languageDTO);
    }

    @PatchMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public LanguageDTO patchLanguage(@PathVariable Long id, @RequestBody LanguageDTO languageDTO) {
        return languageService.saveLanguageByDTO(id, languageDTO);
    }
}
