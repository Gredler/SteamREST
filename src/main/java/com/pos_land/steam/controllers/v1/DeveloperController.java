package com.pos_land.steam.controllers.v1;

import com.pos_land.steam.api.model.DeveloperDTO;
import com.pos_land.steam.services.DeveloperService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = DeveloperController.BASE_URL)
public class DeveloperController {
    public static final String BASE_URL = "api/v1/developer";

    private final DeveloperService developerService;

    public DeveloperController(DeveloperService developerService) {
        this.developerService = developerService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<DeveloperDTO> getAllDevelopers() {
        return developerService.getSortedDevelopers();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DeveloperDTO getDeveloperById(@PathVariable String id) {
        return developerService.getById(new Long(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDeveloper(@PathVariable String id) {
        developerService.deleteById(new Long(id));
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public DeveloperDTO updateDeveloper(@PathVariable Long id, @RequestBody DeveloperDTO developerDTO) {
        return developerService.saveDeveloperByDTO(id, developerDTO);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DeveloperDTO createNewDeveloper(@RequestBody DeveloperDTO developerDTO) {
        return developerService.createNewDeveloper(developerDTO);
    }

    @PatchMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public DeveloperDTO patchDeveloper(@PathVariable Long id, @RequestBody DeveloperDTO developerDTO) {
        return developerService.saveDeveloperByDTO(id, developerDTO);
    }
}
