package com.pos_land.steam.domain;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@Getter
public enum EGenre {

    SIMULATION(1L, "Simulation"),
    RPG(2L, "RPG"),
    MOBA(3L, "MOBA"),
    JRPG(3L, "JRPG"),
    FPS(4L, "GPS"),
    METROIDVANIA(5L, "Simulation"),
    INDIE(6L, "Indie"),
    ACTION(7L, "Action"),
    ADVENTURE(7L, "Adventure"),
    CASUAL(8L, "Casual"),
    MMO(9L, "MMO"),
    STRATEGY(10L, "Strategy");

    private Long id;
    private String name;

    EGenre(Long id, String name) {
        this.id = id;
        this.name = name;
    }


    public static Set<EGenre> getAll() {
        Set<EGenre> allGenres = new HashSet<>();

        allGenres.add(EGenre.ADVENTURE);
        allGenres.add(EGenre.RPG);
        allGenres.add(EGenre.MOBA);
        allGenres.add(EGenre.JRPG);
        allGenres.add(EGenre.SIMULATION);
        allGenres.add(EGenre.FPS);
        allGenres.add(EGenre.METROIDVANIA);
        allGenres.add(EGenre.INDIE);
        allGenres.add(EGenre.ACTION);
        allGenres.add(EGenre.CASUAL);
        allGenres.add(EGenre.MMO);
        allGenres.add(EGenre.STRATEGY);

        return allGenres;
    }
}
