package com.pos_land.steam.domain;


import lombok.Getter;

@Getter
public enum ELanguage {

    ENGLISH(1L, "English"),
    SPANISH(2L, "Spanish"),
    GERMAN(3L, "German");


    private Long id;
    private String name;

    ELanguage(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
