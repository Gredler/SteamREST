package com.pos_land.steam.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String publisherName;

    //todo: find a way to make MapStruct accept recursive stuff
//    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "publisher")
//    Set<Game> games = new HashSet<>();

    public Publisher(String publisherName) {
        this.publisherName = publisherName;
    }

    public Publisher() {
    }

}
