package com.pos_land.steam.api.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DeveloperDTO implements Comparable<DeveloperDTO> {

    private Long id;
    private String developerName;
//    private Set<GameDTO> games = new HashSet<>();


    @Override
    public int compareTo(DeveloperDTO developer) {
        return (this.getId() < developer.getId() ? -1 :
                (this.getId() == developer.getId()) ? 0 : 1);
    }
}
