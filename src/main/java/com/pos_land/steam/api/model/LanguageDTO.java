package com.pos_land.steam.api.model;

import com.pos_land.steam.domain.ELanguage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LanguageDTO {

    private Long id;
    private ELanguage language;
//    private Set<GameDTO> games = new HashSet<>();

}
