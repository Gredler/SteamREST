package com.pos_land.steam.api.mapper;


import com.pos_land.steam.api.model.LanguageDTO;
import com.pos_land.steam.domain.Language;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LanguageMapper {

    LanguageMapper INSTANCE = Mappers.getMapper(LanguageMapper.class);

    LanguageDTO languageToLanguageDTO(Language language);

    Language languageDTOToLanguage(LanguageDTO languageDTO);
}
