package com.pos_land.steam.api.mapper;

import com.pos_land.steam.api.model.DeveloperDTO;
import com.pos_land.steam.domain.Developer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeveloperMapper {

    DeveloperMapper INSTANCE = Mappers.getMapper(DeveloperMapper.class);

    DeveloperDTO developerToDeveloperDTO(Developer developer);

    Developer developerDTOToDeveloper(DeveloperDTO developerDTO);
}
