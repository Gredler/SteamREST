package com.pos_land.steam.api.mapper;

import com.pos_land.steam.api.model.GameDTO;
import com.pos_land.steam.domain.Game;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GameMapper {

    GameMapper INSTANCE = Mappers.getMapper(GameMapper.class);

    GameDTO gameToGameDTO(Game game);

    Game gameDTOToGame(GameDTO gameDTO);
}
