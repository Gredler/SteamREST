package com.pos_land.steam.services;

import com.pos_land.steam.api.model.GameDTO;

import java.util.Set;

public interface GameService {

    Set<GameDTO> getGames();

    Set<GameDTO> getSortedGames();

    GameDTO getById(Long id);

    GameDTO saveGameByDTO(Long id, GameDTO gameDTO);

    GameDTO createNewGame(GameDTO gameDTO);

    void deleteById(Long id);

    Set<GameDTO> getDisplayedGames(Set<GameDTO> domainGames);
}
