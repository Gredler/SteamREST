package com.pos_land.steam.services;

import com.pos_land.steam.api.mapper.UserMapper;
import com.pos_land.steam.api.model.UserDTO;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public Set<UserDTO> getUsers() {
        Set<UserDTO> userDTOSet = new HashSet<>();

        userRepository.findAll().iterator().forEachRemaining(userDomain -> userDTOSet.add(userMapper.userToUserDTO(userDomain)));

        return userDTOSet;
    }

    @Override
    public Set<UserDTO> getSortedUsers() {
        SortedSet<UserDTO> userDTOSet = new TreeSet<>(Comparator.comparing(UserDTO::getId));

        userRepository.findAll().iterator().forEachRemaining(userDomain -> userDTOSet.add(userMapper.userToUserDTO(userDomain)));

        return userDTOSet;
    }

    @Override
    public UserDTO getById(Long id) {
        //get user domain and return converted dto
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new NotFoundException("User with ID " + id + " not found!");
        }

        return userMapper.userToUserDTO(userOptional.get());
    }

    @Override
    public UserDTO saveUserByDTO(Long id, UserDTO userDTO) {
        User user = userMapper.userDTOToUser(userDTO);
        user.setId(id);

        return saveAndReturnDTO(user);
    }

    private UserDTO saveAndReturnDTO(User user) {
        User savedUser = userRepository.save(user);

        UserDTO returnDto = userMapper.userToUserDTO(savedUser);

        return returnDto;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserDTO createNewUser(UserDTO userDTO) {
        return saveAndReturnDTO(userMapper.userDTOToUser(userDTO));
    }
}
