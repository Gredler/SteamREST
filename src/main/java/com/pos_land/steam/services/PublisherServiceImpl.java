package com.pos_land.steam.services;

import com.pos_land.steam.api.mapper.PublisherMapper;
import com.pos_land.steam.api.model.PublisherDTO;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.PublisherRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository publisherRepository;
    private final PublisherMapper publisherMapper;

    public PublisherServiceImpl(PublisherRepository publisherRepository, PublisherMapper publisherMapper) {
        this.publisherRepository = publisherRepository;
        this.publisherMapper = publisherMapper;
    }

    @Override
    public Set<PublisherDTO> getPublishers() {

        Set<PublisherDTO> publisherDTOSet = new HashSet<>();
        publisherRepository.findAll().iterator().forEachRemaining(publisher -> publisherDTOSet.add(publisherMapper.publisherToPublisherDTO(publisher)));


        return publisherDTOSet;
    }

    @Override
    public Set<PublisherDTO> getSortedPublishers() {

        SortedSet<PublisherDTO> publisherDTOSet = new TreeSet<>(Comparator.comparing(PublisherDTO::getId));
        publisherRepository.findAll().iterator().forEachRemaining(publisher -> publisherDTOSet.add(publisherMapper.publisherToPublisherDTO(publisher)));


        return publisherDTOSet;
    }

    @Override
    public PublisherDTO getById(Long id) {

        Optional<Publisher> publisherOptional = publisherRepository.findById(id);

        if (!publisherOptional.isPresent()) {
            throw new NotFoundException("Publisher with ID " + id + " not found!");
        }

        return publisherMapper.publisherToPublisherDTO(publisherOptional.get());
    }

    @Override
    public PublisherDTO savePublisherByDTO(Long id, PublisherDTO publisherDTO) {
        Publisher publisher = publisherMapper.publisherDTOToPublisher(publisherDTO);
        publisher.setId(id);

        return saveAndReturnDTO(publisher);
    }

    private PublisherDTO saveAndReturnDTO(Publisher publisher) {
        Publisher savedPublisher = publisherRepository.save(publisher);

        PublisherDTO returnDto = publisherMapper.publisherToPublisherDTO(savedPublisher);

        return returnDto;
    }

    @Override
    public void deleteById(Long id) {
        publisherRepository.deleteById(id);
    }

    @Override
    public PublisherDTO createNewPublisher(PublisherDTO publisherDTO) {
        return saveAndReturnDTO(publisherMapper.publisherDTOToPublisher(publisherDTO));
    }
}
