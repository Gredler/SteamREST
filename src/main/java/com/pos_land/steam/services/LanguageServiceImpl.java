package com.pos_land.steam.services;

import com.pos_land.steam.api.mapper.LanguageMapper;
import com.pos_land.steam.api.model.LanguageDTO;
import com.pos_land.steam.domain.Language;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.LanguageRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepository languageRepository;
    private final LanguageMapper languageMapper;

    public LanguageServiceImpl(LanguageRepository languageRepository, LanguageMapper languageMapper) {
        this.languageRepository = languageRepository;
        this.languageMapper = languageMapper;
    }

    @Override

    public LanguageDTO getById(Long id) {
        Optional<Language> languageOptional = languageRepository.findById(id);

        if (!languageOptional.isPresent()) {
            throw new NotFoundException("Language with ID " + id + " not found!");
        }

        return languageMapper.languageToLanguageDTO(languageOptional.get());
    }

    @Override
    public Set<LanguageDTO> getLanguages() {
        Set<LanguageDTO> languageDTOS = new HashSet<>();

        languageRepository.findAll().forEach(language -> languageDTOS.add(languageMapper.languageToLanguageDTO(language)));

        return languageDTOS;
    }

    @Override
    public Set<LanguageDTO> getSortedLanguages() {
        SortedSet<LanguageDTO> languageDTOS = new TreeSet<>(Comparator.comparing(LanguageDTO::getId));

        languageRepository.findAll().forEach(language -> languageDTOS.add(languageMapper.languageToLanguageDTO(language)));

        return languageDTOS;
    }

    @Override
    public Set<LanguageDTO> getDisplayedLanguages(Set<LanguageDTO> domainLanguages) {

        Set<LanguageDTO> allLanguages = getLanguages();
        Set<LanguageDTO> displayedLanguages = new HashSet<>();
        allLanguages.iterator().forEachRemaining(languageDTO -> {
            displayedLanguages.add(languageDTO);

            domainLanguages.forEach(language -> {
                if (language.getId().longValue() == languageDTO.getId()) {
                    displayedLanguages.remove(languageDTO);
                }
            });
        });

        return displayedLanguages;
    }

    @Override
    public LanguageDTO saveLanguageByDTO(Long id, LanguageDTO languageDTO) {
        Language language = languageMapper.languageDTOToLanguage(languageDTO);
        language.setId(id);

        return saveAndReturnDTO(language);
    }

    @Override
    public void deleteById(Long id) {
        languageRepository.deleteById(id);
    }

    @Override
    public LanguageDTO createNewLanguage(LanguageDTO languageDTO) {
        return saveAndReturnDTO(languageMapper.languageDTOToLanguage(languageDTO));
    }

    private LanguageDTO saveAndReturnDTO(Language language) {
        Language savedLanguage = languageRepository.save(language);

        LanguageDTO returnDto = languageMapper.languageToLanguageDTO(savedLanguage);

        return returnDto;
    }
}
