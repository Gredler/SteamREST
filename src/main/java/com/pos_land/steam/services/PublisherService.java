package com.pos_land.steam.services;

import com.pos_land.steam.api.model.PublisherDTO;

import java.util.Set;

public interface PublisherService {

    Set<PublisherDTO> getPublishers();

    Set<PublisherDTO> getSortedPublishers();

    PublisherDTO getById(Long id);

    PublisherDTO savePublisherByDTO(Long id, PublisherDTO publisherDTO);

    void deleteById(Long id);

    PublisherDTO createNewPublisher(PublisherDTO publisherDTO);
}
