package com.pos_land.steam.services;

import com.pos_land.steam.api.mapper.GameMapper;
import com.pos_land.steam.api.model.GameDTO;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.GameRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;
    private final GameMapper gameMapper;

    public GameServiceImpl(GameRepository gameRepository, GameMapper gameMapper) {
        this.gameRepository = gameRepository;
        this.gameMapper = gameMapper;
    }

    @Override
    public Set<GameDTO> getGames() {
        Set<GameDTO> gameSet = new HashSet<>();

        gameRepository.findAll().iterator().forEachRemaining(game -> gameSet.add(gameMapper.gameToGameDTO(game)));

        return gameSet;
    }

    @Override
    public Set<GameDTO> getSortedGames() {
        SortedSet<GameDTO> gameSet = new TreeSet<>(Comparator.comparing(GameDTO::getId));

        gameRepository.findAll().iterator().forEachRemaining(game -> gameSet.add(gameMapper.gameToGameDTO(game)));

        return gameSet;
    }


    @Override
    public GameDTO getById(Long id) {

        Optional<Game> game = gameRepository.findById(id);

        if (!game.isPresent()) {
            throw new NotFoundException("Game with ID " + id + " not found!");
        }

        return gameMapper.gameToGameDTO(game.get());
    }

    @Override
    public void deleteById(Long id) {

        gameRepository.deleteById(id);
    }

    @Override
    public Set<GameDTO> getDisplayedGames(Set<GameDTO> domainGames) {

        Set<GameDTO> allGames = getGames();
        Set<GameDTO> displayedGames = new HashSet<>();
        allGames.iterator().forEachRemaining(gameDTO -> {
            displayedGames.add(gameDTO);

            domainGames.forEach(game -> {
                if (game.getId() == gameDTO.getId()) {
                    displayedGames.remove(gameDTO);
                }
            });
        });

        return displayedGames;
    }

    @Override
    public GameDTO saveGameByDTO(Long id, GameDTO gameDTO) {
        Game game = gameMapper.gameDTOToGame(gameDTO);
        game.setId(id);

        return saveAndReturnDTO(game);
    }

    @Override
    public GameDTO createNewGame(GameDTO gameDTO) {
        return saveAndReturnDTO(gameMapper.gameDTOToGame(gameDTO));
    }

    private GameDTO saveAndReturnDTO(Game game) {
        Game savedGame = gameRepository.save(game);

        GameDTO returnDto = gameMapper.gameToGameDTO(savedGame);

        return returnDto;
    }
}
