package com.pos_land.steam.services;

import com.pos_land.steam.api.mapper.DeveloperMapper;
import com.pos_land.steam.api.model.DeveloperDTO;
import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.DeveloperRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    private final DeveloperRepository developerRepository;
    private final DeveloperMapper developerMapper;

    public DeveloperServiceImpl(DeveloperRepository developerRepository, DeveloperMapper developerMapper) {
        this.developerRepository = developerRepository;
        this.developerMapper = developerMapper;
    }

    @Override
    public Set<DeveloperDTO> getDevelopers() {
        Set<DeveloperDTO> developers = new HashSet<>();

        developerRepository.findAll().iterator().forEachRemaining(developer -> developers.add(developerMapper.developerToDeveloperDTO(developer)));

        return developers;
    }

    @Override
    public Set<DeveloperDTO> getSortedDevelopers() {
        SortedSet<DeveloperDTO> developers = new TreeSet<>(Comparator.comparing(DeveloperDTO::getId));
        developerRepository.findAll().iterator().forEachRemaining(developer -> developers.add(developerMapper.developerToDeveloperDTO(developer)));

        return developers;

    }

    @Override
    public DeveloperDTO getById(Long id) {

        Optional<Developer> developerOptional = developerRepository.findById(id);

        if (!developerOptional.isPresent()) {
            throw new NotFoundException("Developer with ID " + id + " not found!");
        }

        return developerMapper.developerToDeveloperDTO(developerOptional.get());
    }


    @Override
    public void deleteById(Long id) {

        developerRepository.deleteById(id);

    }

    @Override
    public Set<DeveloperDTO> getSelectedDevelopers(Set<DeveloperDTO> domainDevelopers) {
        Set<DeveloperDTO> allDevelopers = getDevelopers();
        Set<DeveloperDTO> selectedDevelopers = new HashSet<>();

        allDevelopers.forEach(developerDTO -> {
            selectedDevelopers.add(developerDTO);

            domainDevelopers.forEach(dev -> {
                if (dev.getId().longValue() == developerDTO.getId()) {
                    selectedDevelopers.remove(developerDTO);
                }
            });
        });

        return selectedDevelopers;
    }

    @Override
    public DeveloperDTO saveDeveloperByDTO(Long id, DeveloperDTO developerDTO) {
        Developer developer = developerMapper.developerDTOToDeveloper(developerDTO);
        developer.setId(id);

        return saveAndReturnDTO(developer);
    }

    @Override
    public DeveloperDTO createNewDeveloper(DeveloperDTO developerDTO) {
        return saveAndReturnDTO(developerMapper.developerDTOToDeveloper(developerDTO));
    }

    private DeveloperDTO saveAndReturnDTO(Developer developer) {
        Developer savedDeveloper = developerRepository.save(developer);

        DeveloperDTO returnDto = developerMapper.developerToDeveloperDTO(savedDeveloper);

        return returnDto;
    }
}
