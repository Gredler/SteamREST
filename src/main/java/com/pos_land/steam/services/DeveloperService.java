package com.pos_land.steam.services;

import com.pos_land.steam.api.model.DeveloperDTO;

import java.util.Set;

public interface DeveloperService {

    Set<DeveloperDTO> getDevelopers();

    Set<DeveloperDTO> getSortedDevelopers();

    DeveloperDTO getById(Long id);

    DeveloperDTO saveDeveloperByDTO(Long id, DeveloperDTO developerDTO);

    DeveloperDTO createNewDeveloper(DeveloperDTO developerDTO);

    void deleteById(Long id);

    Set<DeveloperDTO> getSelectedDevelopers(Set<DeveloperDTO> domainDevelopers);
}
