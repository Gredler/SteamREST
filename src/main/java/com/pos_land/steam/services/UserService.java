package com.pos_land.steam.services;

import com.pos_land.steam.api.model.UserDTO;

import java.util.Set;

public interface UserService {

    Set<UserDTO> getUsers();

    Set<UserDTO> getSortedUsers();

    UserDTO getById(Long id);

    UserDTO saveUserByDTO(Long id, UserDTO userDTO);

    void deleteById(Long id);

    UserDTO createNewUser(UserDTO userDTO);

}
