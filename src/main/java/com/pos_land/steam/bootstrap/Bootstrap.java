package com.pos_land.steam.bootstrap;

import com.pos_land.steam.api.mapper.*;
import com.pos_land.steam.api.model.*;
import com.pos_land.steam.domain.*;
import com.pos_land.steam.repositories.*;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.LanguageService;
import com.pos_land.steam.services.PublisherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class Bootstrap implements CommandLineRunner {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    private final GameRepository gameRepository;
    private final GameMapper gameMapper;
    private final GameService gameService;

    private final PublisherRepository publisherRepository;
    private final PublisherService publisherService;
    private final PublisherMapper publisherMapper;

    private final LanguageService languageService;
    private final LanguageRepository languageRepository;
    private final LanguageMapper languageMapper;

    private final DeveloperService developerService;
    private final DeveloperRepository developerRepository;
    private final DeveloperMapper developerMapper;

    public Bootstrap(UserRepository userRepository, UserMapper userMapper, GameRepository gameRepository, GameMapper gameMapper, GameService gameService, PublisherRepository publisherRepository, PublisherService publisherService, PublisherMapper publisherMapper, LanguageService languageService, LanguageRepository languageRepository, LanguageMapper languageMapper, DeveloperService developerService, DeveloperRepository developerRepository, DeveloperMapper developerMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.gameRepository = gameRepository;
        this.gameMapper = gameMapper;
        this.gameService = gameService;
        this.publisherRepository = publisherRepository;
        this.publisherService = publisherService;
        this.publisherMapper = publisherMapper;
        this.languageService = languageService;
        this.languageRepository = languageRepository;
        this.languageMapper = languageMapper;
        this.developerService = developerService;
        this.developerRepository = developerRepository;
        this.developerMapper = developerMapper;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        loadLanguages();
        loadPublishers();
        loadDevelopers();
        loadGames();
        loadUsers();
    }

    private void loadDevelopers() {
        List<Developer> developers = new ArrayList<>(2);

        DeveloperDTO electronicDev = new DeveloperDTO();
        electronicDev.setDeveloperName("Electronic Arts");
        electronicDev.setId(1L);

        developers.add(developerMapper.developerDTOToDeveloper(electronicDev));

        DeveloperDTO jowoodDev = new DeveloperDTO();
        jowoodDev.setDeveloperName("Jowood");
        jowoodDev.setId(2L);

        developers.add(developerMapper.developerDTOToDeveloper(jowoodDev));

        developerRepository.saveAll(developers);
    }

    private void loadLanguages() {
        List<Language> languages = new ArrayList<>();

        LanguageDTO engLanguage = new LanguageDTO();
        engLanguage.setId(1L);
        engLanguage.setLanguage(ELanguage.ENGLISH);

        LanguageDTO gerLanguage = new LanguageDTO();
        gerLanguage.setId(2L);
        gerLanguage.setLanguage(ELanguage.GERMAN);

        LanguageDTO spanLanguage = new LanguageDTO();
        spanLanguage.setId(3L);
        spanLanguage.setLanguage(ELanguage.SPANISH);


        languages.add(languageMapper.languageDTOToLanguage(engLanguage));
        languages.add(languageMapper.languageDTOToLanguage(gerLanguage));
        languages.add(languageMapper.languageDTOToLanguage(spanLanguage));

        languageRepository.saveAll(languages);
    }

    private void loadPublishers() {
        List<Publisher> publisherSet = new ArrayList<>();

        PublisherDTO publisher1 = new PublisherDTO();
        publisher1.setId(1L);
        publisher1.setPublisherName("BANDAI NAMCO");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher1));

        PublisherDTO publisher2 = new PublisherDTO();
        publisher2.setId(2L);
        publisher2.setPublisherName("Blue Byte");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher2));

        PublisherDTO publisher3 = new PublisherDTO();
        publisher3.setId(3L);
        publisher3.setPublisherName("Blizzard");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher3));

        PublisherDTO publisher4 = new PublisherDTO();
        publisher4.setId(4L);
        publisher4.setPublisherName("Daybreak Game Company");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher4));

        PublisherDTO publisher5 = new PublisherDTO();
        publisher5.setId(5L);
        publisher5.setPublisherName("Cloud Imperium Games");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher5));

        PublisherDTO publisher6 = new PublisherDTO();
        publisher6.setId(6L);
        publisher6.setPublisherName("Bluehole Studio");
        publisherSet.add(publisherMapper.publisherDTOToPublisher(publisher6));

        publisherRepository.saveAll(publisherSet);
    }

    private void loadGames() {
        List<Game> games = new ArrayList<>(4);

        GameDTO commandConquerGame = new GameDTO();
        commandConquerGame.setId(1L);
        commandConquerGame.setDescription("A realtime Strategy Game");
        commandConquerGame.setGameName("Command and Conquer 3");
        commandConquerGame.setGenre(EGenre.STRATEGY);
        Set<LanguageDTO> languages = languageService.getLanguages();
        commandConquerGame.setLanguages(languages);
        commandConquerGame.setPrice(29.99);
        commandConquerGame.setPublisher(null);
        Set<DeveloperDTO> developers = developerService.getSortedDevelopers();
        commandConquerGame.setDevelopers(developers);
        commandConquerGame.setReleaseDate(new Date());

        games.add(gameMapper.gameDTOToGame(commandConquerGame));


        GameDTO gothicGame = new GameDTO();
        gothicGame.setId(2L);
        gothicGame.setDescription("A medieval RPG");
        gothicGame.setGameName("Gothic");
        gothicGame.setGenre(EGenre.RPG);
        gothicGame.setLanguages(languages);
        gothicGame.setPrice(9.99);
        gothicGame.setDevelopers(developers);
        gothicGame.setPublisher(publisherService.getById(1L));
        gothicGame.setReleaseDate(new Date());
        games.add(gameMapper.gameDTOToGame(gothicGame));


        GameDTO darkSoulsGame = new GameDTO();
        darkSoulsGame.setId(3L);
        darkSoulsGame.setDescription("A challenging dark RPG game with dozens of bosses");
        darkSoulsGame.setGameName("Dark Souls III");
        darkSoulsGame.setGenre(EGenre.RPG);
        darkSoulsGame.setLanguages(languages);
        darkSoulsGame.setPrice(59.99);
        darkSoulsGame.setPublisher(publisherService.getById(2L));
        darkSoulsGame.setDevelopers(developers);

        darkSoulsGame.setReleaseDate(new Date());

        games.add(gameMapper.gameDTOToGame(darkSoulsGame));

        gameRepository.saveAll(games);
    }

    private void loadUsers() {
        List<User> users = new ArrayList<>(2);

        UserDTO andiUser = new UserDTO();
        andiUser.setId(1L);
        andiUser.setEmail("andimurk@hotmail.de");
        andiUser.setUserName("AndiM202");
        andiUser.setPassword("123456");
        andiUser.getGames().add(gameService.getById(1L));
        andiUser.getGames().add(gameService.getById(2L));
        andiUser.getGames().add(gameService.getById(3L));

        users.add(userMapper.userDTOToUser(andiUser));

        UserDTO gredlerUser = new UserDTO();
        gredlerUser.setId(2L);
        gredlerUser.setEmail("angredler@tsn.at");
        gredlerUser.setUserName("Volecheck");
        gredlerUser.setPassword("12345678");
        users.add(userMapper.userDTOToUser(gredlerUser));

        UserDTO lolleUser = new UserDTO();
        lolleUser.setId(3L);
        lolleUser.setEmail("lbroger@tsn.at");
        lolleUser.setUserName("Lolle");
        lolleUser.setPassword("12345678");

        users.add(userMapper.userDTOToUser(lolleUser));

        userRepository.saveAll(users);
    }
}
