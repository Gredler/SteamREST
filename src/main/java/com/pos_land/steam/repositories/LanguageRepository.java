package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Language;
import org.springframework.data.repository.CrudRepository;

public interface LanguageRepository extends CrudRepository<Language, Long> {
}
