package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
