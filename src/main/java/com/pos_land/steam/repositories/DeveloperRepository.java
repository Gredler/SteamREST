package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Developer;
import org.springframework.data.repository.CrudRepository;


public interface DeveloperRepository extends CrudRepository<Developer, Long> {


}
