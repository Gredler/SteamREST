package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
